# Testing app

This project is to test `@testing-library/react-native` with redux state & graphQL

# Libraries 💻

- Redux (`@reduxjs/toolkit`)
- Redux Persistant
- React Navigation
- Testing library (`@testing-library/react-native`)
- Apollo client (`@apollo/client`)
- QR Generator(`react-native-qrcode-svg`)
- Formik (`formik`)

# Review Jest setup ⭐️

Supports:

- Redux with persist data in Async Storage
- React Navigation
- GraphQL (with `cross-fetch`)

# How to run app 🚀

1. Clone this repo
2. `yarn install`
3. `yarn android` or `yarn ios`

# How to run tests 🥳🥳

1. `yarn test`

![test](screenshots/test.png)

## Test on:

### Home

- Simple Login
- Press button to add in count
- Log out

### Formik

- Submit without fill required inputs
- Fill input and submit

### Navigation

- Login and navigate to Home screen

### GraphQL

- Loading state on request
- Error state on request
- Render elements on Flatlist

### Voice Search with GraphQL

- Try searching for an character
