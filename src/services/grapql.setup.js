import 'cross-fetch/polyfill';
import { ApolloClient, InMemoryCache } from '@apollo/client';

const GraphqlClient = new ApolloClient({
	uri: 'https://rickandmortyapi.com/graphql',
	cache: new InMemoryCache({
		typePolicies: {
			Query: {
				fields: {
					characters: {
						keyArgs: ['results'],
						merge(existing = [], incoming, { variables }) {
							const exitsResult = existing?.results || [];
							const incomingResult = incoming?.results || [];
							if (variables?.noMerge) {
								return {
									results: incomingResult,
								};
							}
							return {
								info: incoming.info,
								results: [...exitsResult, ...incomingResult],
							};
						},
					},
				},
			},
		},
	}),
});

export default GraphqlClient;
