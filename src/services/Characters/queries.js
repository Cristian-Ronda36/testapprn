import { gql } from '@apollo/client';

export const GET_CHARACTERS_PAGINATION = gql`
	query getCharacter($page: Int!) {
		characters(page: $page) {
			info {
				pages
			}
			results {
				name
				id
				status
				species
				image
			}
		}
	}
`;

export const GET_CHARACTERS_BY_NAME = gql`
	query getCharacterByName($name: String!) {
		characters(filter: { name: $name }) {
			results {
				name
				id
				status
				species
				image
			}
		}
	}
`;
