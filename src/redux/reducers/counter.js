import {createSlice} from '@reduxjs/toolkit';

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 0,
  },
  reducers: {
    increment: state => {
      const newValue = state.value + 1;
      if (newValue > 20) {
        state.value;
      } else {
        state.value = newValue;
      }
    },
    decrement: state => {
      const newValue = state.value - 1;
      if (newValue < 0) {
        state.value;
      } else {
        state.value = newValue;
      }
    },
    reset: state => {
      state.value = 0;
    },
    incrementByAmount: (state, action) => {
      const newValue = state.value + action.payload;
      if (newValue > 20) {
        state.value;
      } else {
        state.value = newValue;
      }
    },
  },
});

export const {increment, decrement, incrementByAmount, reset} =
  counterSlice.actions;

export default counterSlice.reducer;
