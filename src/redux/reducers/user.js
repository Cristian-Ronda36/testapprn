import {createSlice} from '@reduxjs/toolkit';

export const counterSlice = createSlice({
  name: 'user',
  initialState: {
    userData: null,
  },
  reducers: {
    loginSuccess: (state, action) => {
      state.userData = action.payload;
    },
    logoutSuccess: (state, action) => {
      state.userData = null;
    },
  },
});

export const {loginSuccess, logoutSuccess} = counterSlice.actions;

// Actions

export const login = () => async dispatch => {
  try {
    const res = await fetch('https://randomuser.me/api/');
    if (res.status >= 400) {
      throw new Error();
    }
    const {results} = await res.json();
    dispatch(loginSuccess(results[0]));
  } catch (e) {
    console.error(e.message);
    return null;
  }
};

export const logout = () => async dispatch => {
  try {
    return dispatch(logoutSuccess());
  } catch (e) {
    return console.error(e.message);
  }
};

export default counterSlice.reducer;
