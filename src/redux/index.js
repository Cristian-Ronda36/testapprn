import { configureStore } from '@reduxjs/toolkit';
import counterReducer from './reducers/counter';
import userReducer from './reducers/user';
import { combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import thunkMiddleware from 'redux-thunk';

const persistConfig = {
	key: 'root',
	storage: AsyncStorage,
	whitelist: ['userReducer'],
};

const reducer = combineReducers({
	counterReducer,
	userReducer,
});
const persistedReducer = persistReducer(persistConfig, reducer);
export const store = configureStore({
	reducer: persistedReducer,
	middleware: [thunkMiddleware],
});
export const persistor = persistStore(store);
