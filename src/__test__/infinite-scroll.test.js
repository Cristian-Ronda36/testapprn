import * as React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import CharacterScreen from '../screens/Character';
import GrapqlTestProvider from './components/GrapqlTestProvider';
import { act } from 'react-test-renderer';
import {
	characterMocksError,
	characterMocksSuccess,
	characters,
} from '../__mocks__/data/Characters.mock';
import { scrollEvent } from './utils';

const CharacterSuccess = (
	<GrapqlTestProvider mocks={characterMocksSuccess}>
		<CharacterScreen />
	</GrapqlTestProvider>
);

const CharacterError = (
	<GrapqlTestProvider mocks={characterMocksError}>
		<CharacterScreen />
	</GrapqlTestProvider>
);
jest.setTimeout(30000);

describe('Should can scroll screen', () => {
	test('should show loading', async () => {
		const { getByTestId, unmount } = render(CharacterSuccess);
		const loadingElement = getByTestId('test-loading-character');
		expect(loadingElement).toBeTruthy();
		unmount();
	});
	test('should show error', async () => {
		const { getByTestId, unmount } = render(CharacterError);
		await act(() => new Promise((resolve) => setTimeout(resolve, 0)));
		const errorElement = getByTestId('test-error-character');
		expect(errorElement).toBeTruthy();
		unmount();
	});
	test('should show characters', async () => {
		const { getByTestId, unmount, getByText } = render(CharacterSuccess);
		await act(() => new Promise((resolve) => setTimeout(resolve, 0)));
		const listElement = getByTestId('test-list-characters');
		expect(listElement.props.data.length).toBe(characters.length);
		expect(listElement).toBeTruthy();

		fireEvent.scroll(listElement, scrollEvent);
		expect(getByText(/Abradolf Lincler/i)).toBeTruthy();

		unmount();
	});
});
