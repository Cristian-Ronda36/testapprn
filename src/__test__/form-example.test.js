import * as React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import ExampleForm from '../screens/ExampleForm';
import { act } from 'react-test-renderer';

describe('Shourd can submit in register form', () => {
	const handleSubmit = jest.fn();
	it('should not run submitFunction and show label "Required" in inputs', async () => {
		const { getByTestId } = render(<ExampleForm onSubmit={handleSubmit} />);
		const buttonSubmit = getByTestId('test-button-submit');

		await act(async () => {
			fireEvent.press(buttonSubmit);
		});
		const errorName = getByTestId('test-input-name-error');
		const errorEmail = getByTestId('test-input-email-error');
		const errorNumber = getByTestId('test-input-number-error');
		expect(errorName.props.children).not.toBeNull();
		expect(errorName.props.children).toBe('Required');
		expect(errorEmail.props.children).toBe('Email required');
		expect(errorNumber.props.children).toBe('Required');
		expect(handleSubmit).toHaveBeenCalledTimes(0);
	});

	it('should can see name, email, and phoneNumber', async () => {
		const { getByTestId } = render(<ExampleForm onSubmit={handleSubmit} />);
		const input = {
			name: 'Mario Ralm',
			email: 'example@gmail.com',
			phoneNumber: '1231231234',
		};
		const formatNumber = '123-123-1234';

		const buttonSubmit = getByTestId('test-button-submit');
		const inputName = getByTestId('test-input-name');
		const inputEmail = getByTestId('test-input-email');
		const inputNumber = getByTestId('test-input-number');
		await act(async () => {
			fireEvent.changeText(inputName, input.name);
		});
		await act(async () => {
			fireEvent.changeText(inputEmail, input.email);
		});
		await act(async () => {
			fireEvent.changeText(inputNumber, input.phoneNumber);
		});
		await act(async () => {
			fireEvent.press(buttonSubmit);
		});

		expect(inputName.props.value).toBe(input.name);
		expect(inputEmail.props.value).toBe(input.email);
		expect(inputNumber.props.value).toBe(formatNumber);

		expect(handleSubmit).toHaveBeenCalledWith({
			...input,
			phoneNumber: formatNumber,
		});
	});
});
