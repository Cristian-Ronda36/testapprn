import * as React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import { store } from '../redux';
import Login from '../screens/Login';
import Home from '../screens/Home';
import ReduxTestProvider from './components/ReduxTestProvider';

jest.mock('redux-persist', () => {
	const real = jest.requireActual('redux-persist');
	return {
		...real,
		persistReducer: jest
			.fn()
			.mockImplementation((config, reducers) => reducers),
	};
});
fetch = jest.fn(() =>
	Promise.resolve({
		ok: true,
		status: 200,
		json: () => ({
			results: [{ name: { first: 'Hola' } }],
		}),
	}),
);

const HomeScreen = (
	<ReduxTestProvider>
		<Home />
	</ReduxTestProvider>
);
const LoginScreen = (
	<ReduxTestProvider>
		<Login />
	</ReduxTestProvider>
);

describe('Login flow and add 3 in counter ', () => {
	test('should login success', async () => {
		const { getByTestId } = render(LoginScreen);
		await fireEvent.press(getByTestId('test-login-button'));
		const userState = store.getState().userReducer;
		expect(userState.userData).not.toBeNull();
	});

	test('sould add +3 when the button is pressed', () => {
		const { getByText } = render(HomeScreen);

		fireEvent.press(getByText('+'));
		fireEvent.press(getByText('+'));
		fireEvent.press(getByText('+'));

		const counterState = store.getState().counterReducer;

		expect(counterState).toEqual(
			expect.objectContaining({
				value: 3,
			}),
		);
	});

	test('logout', async () => {
		const { getByTestId } = render(HomeScreen);

		await fireEvent.press(getByTestId('test-logout-button'));

		const userState = store.getState().userReducer;

		expect(userState).toEqual(
			expect.objectContaining({
				userData: null,
			}),
		);
	});
});
