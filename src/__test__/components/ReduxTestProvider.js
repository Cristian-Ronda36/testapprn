import * as React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../redux';

const ReduxTestProvider = ({ children }) => (
	<Provider store={store}>{children}</Provider>
);

export default ReduxTestProvider;
