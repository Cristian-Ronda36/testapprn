import React from 'react';

import { MockedProvider } from '@apollo/client/testing';

const GrapqlTestProvider = ({ children, mocks, addTypename = false }) => (
	<MockedProvider addTypename={addTypename} mocks={mocks}>
		{children}
	</MockedProvider>
);
export default GrapqlTestProvider;
