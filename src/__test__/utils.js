export const scrollEvent = {
	nativeEvent: {
		contentOffset: {
			x: 0,
			y: 125,
		},
		contentSize: {
			// Dimensions of the scrollable content
			height: 885,
			width: 328,
		},
		layoutMeasurement: {
			// Dimensions of the device
			height: 469,
			width: 328,
		},
	},
};
