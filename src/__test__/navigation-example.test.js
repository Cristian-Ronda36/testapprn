import React from 'react';
import ReduxTestProvider from './components/ReduxTestProvider';
import { store } from '../redux';
import { MainNavigation } from '../App';
import { act, fireEvent, render, cleanup } from '@testing-library/react-native';

afterEach(cleanup);

jest.mock('redux-persist', () => {
	const real = jest.requireActual('redux-persist');
	return {
		...real,
		persistReducer: jest
			.fn()
			.mockImplementation((config, reducers) => reducers),
	};
});
const Navigation = (
	<ReduxTestProvider>
		<MainNavigation />
	</ReduxTestProvider>
);
describe('Should render a new screen', () => {
	test('should login success', async () => {
		const { getByTestId } = render(Navigation);
		await act(() => fireEvent.press(getByTestId('test-login-button')));
		const userState = store.getState().userReducer;
		expect(userState.userData).not.toBeNull();
	});
	test('should navigate to form and go back', async () => {
		const { getByTestId, getByText } = render(Navigation);
		fireEvent.press(getByTestId('test-form-button'));
		const titlePage = getByText('ExampleForm');
		expect(titlePage.children).toBeTruthy();
	});
});
