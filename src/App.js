import React from 'react';
import { store, persistor } from '@redux';
import { Provider, useSelector } from 'react-redux';
import {
	createStackNavigator,
	TransitionPresets,
} from '@react-navigation/stack';
import Login from './screens/Login';
import Home from './screens/Home';
import ExampleForm from './screens/ExampleForm';
import Camera from './screens/Camera';
import Character from './screens/Character';
import QRGenerator from './screens/QRGenerator';
import Tabs from './screens/Tabs';
import Speech from './screens/Speech';
import { NavigationContainer } from '@react-navigation/native';
import { PersistGate } from 'redux-persist/integration/react';
import { ApolloProvider } from '@apollo/client';
import GraphqlClient from './services/grapql.setup';

const AuthStack = createStackNavigator();
const HomeStack = createStackNavigator();

export const NavigationAuth = () => (
	<AuthStack.Navigator>
		<AuthStack.Screen name="Login" component={Login} />
	</AuthStack.Navigator>
);
export const NavigationHome = () => (
	<HomeStack.Navigator
		screenOptions={{
			gestureEnabled: true,
			...TransitionPresets.SlideFromRightIOS,
		}}>
		<HomeStack.Screen name="Home" component={Home} />
		<HomeStack.Screen name="ExampleForm" component={ExampleForm} />
		<HomeStack.Screen name="Camera" component={Camera} />
		<HomeStack.Screen name="Character" component={Character} />
		<HomeStack.Screen name="QRGenerator" component={QRGenerator} />
		<HomeStack.Screen
			name="TabsExample"
			component={Tabs}
			options={{ header: () => null }}
		/>
		<HomeStack.Screen name="Speech" component={Speech} />
	</HomeStack.Navigator>
);

export const MainNavigation = () => {
	const { userReducer } = useSelector((state) => state);

	return (
		<NavigationContainer>
			{userReducer?.userData ? <NavigationHome /> : <NavigationAuth />}
		</NavigationContainer>
	);
};

const App = () => {
	return (
		<ApolloProvider client={GraphqlClient}>
			<Provider store={store}>
				<PersistGate persistor={persistor} loading={null}>
					<MainNavigation />
				</PersistGate>
			</Provider>
		</ApolloProvider>
	);
};

export default App;
