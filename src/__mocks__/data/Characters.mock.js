import { GET_CHARACTERS_PAGINATION } from '../../services/Characters/queries';

export const characters = [
	{
		name: 'Rick Sanchez',
		id: '1',
		status: 'Alive',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
	},
	{
		name: 'Morty Smith',
		id: '2',
		status: 'Alive',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/2.jpeg',
	},
	{
		name: 'Summer Smith',
		id: '3',
		status: 'Alive',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/3.jpeg',
	},
	{
		name: 'Beth Smith',
		id: '4',
		status: 'Alive',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/4.jpeg',
	},
	{
		name: 'Jerry Smith',
		id: '5',
		status: 'Alive',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/5.jpeg',
	},
	{
		name: 'Abadango Cluster Princess',
		id: '6',
		status: 'Alive',
		species: 'Alien',
		image: 'https://rickandmortyapi.com/api/character/avatar/6.jpeg',
	},
	{
		name: 'Abradolf Lincler',
		id: '7',
		status: 'unknown',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/7.jpeg',
	},
	{
		name: 'Adjudicator Rick',
		id: '8',
		status: 'Dead',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/8.jpeg',
	},
	{
		name: 'Agency Director',
		id: '9',
		status: 'Dead',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/9.jpeg',
	},
	{
		name: 'Alan Rails',
		id: '10',
		status: 'Dead',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/10.jpeg',
	},
	{
		name: 'Albert Einstein',
		id: '11',
		status: 'Dead',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/11.jpeg',
	},
	{
		name: 'Alexander',
		id: '12',
		status: 'Dead',
		species: 'Human',
		image: 'https://rickandmortyapi.com/api/character/avatar/12.jpeg',
	},
	{
		name: 'Alien Googah',
		id: '13',
		status: 'unknown',
		species: 'Alien',
		image: 'https://rickandmortyapi.com/api/character/avatar/13.jpeg',
	},
	{
		name: 'Alien Morty',
		id: '14',
		status: 'unknown',
		species: 'Alien',
		image: 'https://rickandmortyapi.com/api/character/avatar/14.jpeg',
	},
	{
		name: 'Alien Rick',
		id: '15',
		status: 'unknown',
		species: 'Alien',
		image: 'https://rickandmortyapi.com/api/character/avatar/15.jpeg',
	},
];

export const characterMocksSuccess = [
	{
		request: {
			query: GET_CHARACTERS_PAGINATION,
			variables: {
				page: 1,
			},
		},
		result: {
			data: {
				characters: {
					info: { pages: 1 },
					results: characters,
				},
			},
		},
	},
];
export const characterMocksError = [
	{
		request: {
			query: GET_CHARACTERS_PAGINATION,
			variables: {
				page: 1,
			},
		},
		error: new Error('An error occurred'),
	},
];
