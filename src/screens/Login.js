import React from 'react';
import { Pressable, Text } from 'react-native';
import { useDispatch } from 'react-redux';
import Container from '../components/Container';
import { login } from '../redux/reducers/user';
import styles from '../style';
const Login = () => {
	const dispatch = useDispatch();
	return (
		<Container center style={{ alignItems: 'center' }}>
			<Pressable
				style={styles.button}
				onPress={() => dispatch(login())}
				testID="test-login-button">
				<Text style={styles.buttonText}>Login</Text>
			</Pressable>
		</Container>
	);
};

export default Login;
