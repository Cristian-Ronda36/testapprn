import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../redux/reducers/user';
import {
	increment,
	decrement,
	incrementByAmount,
} from '../redux/reducers/counter';
import styles from '../style';
import Container from '../components/Container';
import Button from '../components/Button';
import Colors from '../style/colors';

const Home = ({ navigation }) => {
	const dispatch = useDispatch();
	const { userData } = useSelector(({ userReducer }) => userReducer);
	const { value } = useSelector(({ counterReducer }) => counterReducer);
	if (!userData) {
		return <Text>No data</Text>;
	}
	return (
		<Container style={customStyle.container}>
			<Text style={styles.title}>Welcome! {userData.name.first} </Text>
			<Button
				style={styles.button}
				testID="test-logout-button"
				title="Logout"
				onPress={() => dispatch(logout())}
			/>
			<Button
				title="Go to Form"
				testID="test-form-button"
				style={styles.button}
				onPress={() => navigation.push('ExampleForm')}
			/>
			<Button
				style={styles.button}
				title="Open camera"
				onPress={() => navigation.push('Camera')}
			/>
			<Button
				style={styles.button}
				testID="test-graphql-button"
				title="Open GraphQL example"
				onPress={() => navigation.push('Character')}
			/>
			<Button
				style={styles.button}
				title="Open QR example"
				onPress={() => navigation.push('QRGenerator')}
			/>
			<Button
				style={styles.button}
				title="Tabs example"
				onPress={() => navigation.push('TabsExample')}
			/>
			<Button
				style={styles.button}
				title="Speech example"
				onPress={() => navigation.push('Speech')}
			/>
			<Text style={styles.title}>{value}</Text>
			<View style={[styles.row]}>
				<Button color="black" onPress={() => dispatch(increment())} title="+" />
				<Button
					color="black"
					onPress={() => dispatch(incrementByAmount(5))}
					title="+5"
				/>
				<Button color="black" onPress={() => dispatch(decrement())} title="-" />
			</View>
		</Container>
	);
};

export default Home;

const customStyle = StyleSheet.create({
	container: {
		flex: 1,
		padding: 25,
		backgroundColor: Colors.backgroundContainer,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
