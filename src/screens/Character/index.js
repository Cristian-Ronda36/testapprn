import { useQuery } from '@apollo/client';
import React, { useRef, useState } from 'react';
import {
	FlatList,
	Text,
	ActivityIndicator,
	Pressable,
	StyleSheet,
} from 'react-native';
import Container from '../../components/Container';
import Card from '../../components/Card';
import { GET_CHARACTERS_PAGINATION } from '../../services/Characters/queries';

const CharacterList = () => {
	const [page, setPage] = useState(1);
	const listRef = useRef();
	const { data, loading, error, fetchMore, networkStatus, refetch } = useQuery(
		GET_CHARACTERS_PAGINATION,
		{ variables: { page: 1 } },
	);

	if (loading) {
		return <ActivityIndicator testID="test-loading-character" />;
	}
	if (error) {
		return <Text testID="test-error-character">Error</Text>;
	}

	if (data?.characters) {
		const limit = data.characters.info.pages;
		const toTop = () =>
			listRef.current &&
			listRef.current.scrollToOffset({ animated: true, offset: 0 });
		return (
			<Container testID="test-container-characters">
				<FlatList
					testID="test-list-characters"
					ref={listRef}
					keyExtractor={(item, i) => `${i}-${item.id}`}
					showsVerticalScrollIndicator={false}
					data={data.characters.results}
					renderItem={({ item }) => <Card {...item} />}
					onRefresh={() => {
						refetch();
						setPage(1);
					}}
					refreshing={networkStatus === 4}
					onEndReachedThreshold={0.9}
					onEndReached={() => {
						const isIndexLimit = page + 1 >= limit;
						if (isIndexLimit) {
							return;
						}
						const nextPage = isIndexLimit >= limit ? limit : page + 1;
						setPage(nextPage);
						fetchMore({
							variables: { page: nextPage },
						});
					}}
				/>
				<Pressable onPress={toTop} style={styles.fabButton}>
					<Text style={styles.text}>Up</Text>
				</Pressable>
			</Container>
		);
	}
	return <Text>Sin datos</Text>;
};

export default CharacterList;

const styles = StyleSheet.create({
	fabButton: {
		position: 'absolute',
		bottom: 16,
		right: 16,
		backgroundColor: 'green',
		padding: 14,
		borderRadius: 45,
	},
	text: {
		color: 'white',
		fontWeight: '600',
		fontSize: 18,
	},
});
