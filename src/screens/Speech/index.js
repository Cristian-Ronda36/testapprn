import React, { useEffect } from 'react';
import Container from '../../components/Container';
import useVoiceSearch from '@hooks/useVoiceSearch';
import VoiceInput from '@components/VoiceInput';
import IconButton from '@components/IconButton';
import {
	ActivityIndicator,
	FlatList,
	Modal,
	StyleSheet,
	Text,
	View,
} from 'react-native';
import { useLazyQuery } from '@apollo/client';
import { GET_CHARACTERS_BY_NAME } from '../../services/Characters/queries';
import Card from '../../components/Card';

const App = () => {
	const { onRecordVoice, isRecord, searchText, setSearchText, hasPermission } =
		useVoiceSearch();

	const [getData, { data, loading }] = useLazyQuery(GET_CHARACTERS_BY_NAME, {
		fetchPolicy: 'network-only',
	});

	useEffect(() => {
		let delayDebounceFn;
		if (searchText) {
			delayDebounceFn = setTimeout(() => {
				getData({
					variables: {
						name: searchText,
						noMerge: true,
					},
				});
			}, 1200);
		}
		return () => clearTimeout(delayDebounceFn);
	}, [getData, searchText]);

	return (
		<Container>
			<VoiceInput
				value={searchText}
				placeholder="Apples, oranges..."
				hintText="Search meal"
				onChangeText={(value) => setSearchText(value)}
				disabledVoiceSearch={!hasPermission}
				isRecording={isRecord}
				onRecordVoice={onRecordVoice}
			/>
			<FlatList
				testID="test-list-characters"
				ListEmptyComponent={
					loading ? (
						<ActivityIndicator size={64} color="red" />
					) : (
						<Text style={[styles.title, { color: 'black' }]}>
							Try searching for an item
						</Text>
					)
				}
				numColumns={2}
				keyExtractor={(item, i) => `${i}-${item.id}`}
				showsVerticalScrollIndicator={false}
				data={data?.characters?.results}
				renderItem={({ item }) => <Card {...item} />}
			/>
			<Modal animationType="fade" transparent={true} visible={isRecord}>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<ActivityIndicator size={64} color="red" />
						<Text style={styles.title}>Say</Text>
						<Text style={styles.subtitle}>Apple, oranges or lemons</Text>
					</View>
					<IconButton
						onPress={onRecordVoice}
						style={styles.closeIcon}
						name="close"
					/>
				</View>
			</Modal>
		</Container>
	);
};

export default App;

const styles = StyleSheet.create({
	centeredView: {
		position: 'relative',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#00000022',
	},
	modalView: {
		margin: 20,
		backgroundColor: 'transparent',
		borderRadius: 20,
		padding: 35,
		alignItems: 'center',
	},
	title: {
		fontSize: 24,
		fontWeight: '700',
		textAlign: 'center',
		color: 'white',
		marginBottom: 4,
	},
	subtitle: {
		fontSize: 18,
		fontWeight: '400',
		textAlign: 'center',
		color: '#00000088',
		marginBottom: 4,
	},
	closeIcon: {
		position: 'absolute',
		top: 24,
		right: 24,
		backgroundColor: '#ffffff',
		borderRadius: 45,
	},
});
