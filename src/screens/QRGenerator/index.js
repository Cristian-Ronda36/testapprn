import React, { useState } from 'react';
import Container from '../../components/Container';
import QRCode from 'react-native-qrcode-svg';
import { View } from 'react-native';

const QRGenerator = () => {
	const [state, setWidth] = useState(100);

	const onLayout = (event) => {
		const { width } = event.nativeEvent.layout;
		setWidth(width > 150 ? 150 : width);
	};

	return (
		<Container style={{ flexDirection: 'row' }}>
			<View
				onLayout={onLayout}
				style={{
					flex: 1,
					justifyContent: 'flex-end',
					alignItems: 'center',
				}}>
				<QRCode
					value="https://www.cristian-ronda.tk/"
					size={state * 0.9}
					ecl="Q"
				/>
			</View>
			<View style={{ flex: 1, backgroundColor: 'green' }} />
			<View style={{ flex: 1, backgroundColor: 'blue' }} />
		</Container>
	);
};
export default QRGenerator;
