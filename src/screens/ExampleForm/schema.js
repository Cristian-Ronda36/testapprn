import * as Yup from 'yup';

const RegisterSchema = () =>
	Yup.object().shape({
		name: Yup.string().required('Required'),
		email: Yup.string().email('Email not valid').required('Email required'),
		phoneNumber: Yup.string()
			.matches(/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/, 'Not valid')
			.required('Required'),
	});

export default RegisterSchema;
