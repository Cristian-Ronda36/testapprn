import React from 'react';
import Input from '../../components/Input';
import Container from '../../components/Container';
import { useFormik } from 'formik';
import { Button } from 'react-native';
import RegisterSchema from './schema';

const ExampleForm = ({ onSubmit = () => {} }) => {
	const { values, setFieldValue, handleSubmit, errors } = useFormik({
		initialValues: {
			name: '',
			phoneNumber: '',
			email: '',
		},
		validationSchema: RegisterSchema,
		// validateOnMount: false,
		// validateOnBlur: false,
		validateOnChange: false,
		onSubmit: (values, { validateForm }) => {
			validateForm();
			onSubmit(values);
		},
	});

	return (
		<Container center>
			<Input
				testID="test-input-name"
				value={values.name}
				placeholder="Mario Perls"
				hintText="Your name"
				onChangeText={(value) => setFieldValue('name', value)}
				errorText={errors.name}
				isError={Boolean(errors.name)}
			/>
			<Input
				testID="test-input-email"
				value={values.email}
				placeholder="example@example.com"
				hintText="Your email"
				onChangeText={(value) => setFieldValue('email', value)}
				errorText={errors.email}
				isError={Boolean(errors.email)}
			/>
			<Input
				testID="test-input-number"
				value={values.phoneNumber}
				countryCode="US"
				hintText="Your number"
				placeholder="8765635326"
				textContentType="telephoneNumber"
				keyboardType="numeric"
				onChangeText={(value) => setFieldValue('phoneNumber', value)}
				countryCallingCode="1"
				maxLength={12}
				errorText={errors.phoneNumber}
				isError={Boolean(errors.phoneNumber)}
			/>

			<Button
				title="Submit"
				testID="test-button-submit"
				onPress={handleSubmit}
			/>
		</Container>
	);
};
export default ExampleForm;
