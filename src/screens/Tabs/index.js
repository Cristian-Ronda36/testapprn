import React, { useState } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Container from '../../components/Container';

const Tab = createMaterialTopTabNavigator();

const HomeScreen = () => {
	return (
		<Container>
			<Text>HomeScreen</Text>
		</Container>
	);
};

const SettingsScreen = () => {
	return (
		<Container>
			<Text>SettingsScreen</Text>
		</Container>
	);
};

const DetailsScreen = () => {
	return (
		<Container>
			<Text>DetailsScreen</Text>
		</Container>
	);
};
const WIDTH_INDICATOR = 22;
const routes = [
	{
		label: 'Grocery',
		route: 'Name1',
	},
	{
		label: 'Data',
		route: 'Name2',
	},
	{
		label: 'Map',
		route: 'Name3',
	},
	{
		label: 'Info',
		route: 'Name4',
	},
];
const MyTabs = () => {
	const [widthContainer, setWidthContainer] = useState(0);
	const [widthColumn, setWidthOfColumn] = useState(100);

	const onLayoutColumn = ({
		nativeEvent: {
			layout: { width },
		},
	}) => setWidthOfColumn(width);

	const onLayoutContainer = ({
		nativeEvent: {
			layout: { width },
		},
	}) => setWidthContainer(width);

	const leftIndicator = (widthColumn * 0.9) / 6 - WIDTH_INDICATOR / 2;
	const imageWidth = widthContainer - widthColumn - 10;

	return (
		<>
			<View style={styles.customNav} onLayout={onLayoutContainer}>
				<Image
					style={[
						{
							minWidth: 100,
							width: imageWidth,
							maxHeight: 140,
							height: 50,
							marginHorizontal: 8,
						},
					]}
					source={{ uri: 'https://dummyimage.com/120x64.jpg' }}
				/>
				{routes.map((item) => (
					<TouchableOpacity
						key={item.route}
						style={styles.customNavText}
						onPress={() => alert(`Navigate to ${item.label}`)}>
						<Text>{item.label}</Text>
					</TouchableOpacity>
				))}
			</View>
			<View style={styles.container}>
				<View style={styles.leftBar} />
				<View style={styles.column} onLayout={onLayoutColumn}>
					<View style={styles.header} />
					<Tab.Navigator
						tabBarOptions={{
							pressOpacity: 6,
							activeTintColor: 'black',
							pressColor: 'green',
							inactiveTintColor: '#f0000077',
							bounces: false,
							labelStyle: {
								textAlign: 'left',
								fontSize: 11,
							},
							style: {
								width: '90%',
								height: 45,
								shadowColor: 'transparent',
								elevation: 0,
							},
							indicatorStyle: {
								backgroundColor: 'purple',
								width: WIDTH_INDICATOR,
								left: leftIndicator,
							},
						}}
						style={{ backgroundColor: 'white' }}>
						<Tab.Screen
							name="Homes"
							component={HomeScreen}
							options={{
								title: 'Details',
							}}
						/>
						<Tab.Screen
							name="Settings"
							component={SettingsScreen}
							options={{
								title: 'Variations',
							}}
						/>
						<Tab.Screen
							name="DetailsScreen"
							component={DetailsScreen}
							options={{
								title: 'About',
							}}
						/>
					</Tab.Navigator>
				</View>
			</View>
		</>
	);
};

export default MyTabs;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
	},
	leftBar: {
		backgroundColor: '#a7a263a1',
		flex: 0.2,
		maxWidth: 150,
	},
	column: {
		flex: 1,
	},
	header: {
		backgroundColor: 'cyan',
		height: 80,
	},
	customNav: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		alignItems: 'center',
		height: 64,
		backgroundColor: '#4df16671',
		paddingVertical: 8,
	},
	customNavText: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'flex-start',
		paddingLeft: 8,
		marginHorizontal: 8,
		height: '99%',
	},
});
