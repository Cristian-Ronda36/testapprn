import Voice from '@react-native-voice/voice';
import { useCallback, useEffect, useState } from 'react';
import { Alert, PermissionsAndroid, Platform } from 'react-native';

const useSpeechSearch = () => {
	const [searchText, setSearchText] = useState('');

	const [isRecord, setIsRecord] = useState(false);
	const [hasPermission, setPermission] = useState(false);

	const verifyPermissions = async () => {
		try {
			if (Platform.OS === 'android') {
				const granted = await PermissionsAndroid.request(
					PermissionsAndroid.PERMISSIONS.CAMERA,
					{
						title: 'Permission to access your microphone',
						message:
							'To perform voice searches, access to the microphone is required',
						buttonNeutral: 'Ask Me Later',
						buttonNegative: 'Cancel',
						buttonPositive: 'OK',
					},
				);
				console.log('mededjajs', granted);

				if (granted === PermissionsAndroid.RESULTS.GRANTED) {
					setPermission(true);
				} else {
					Alert.alert(
						'Need permissions to access',
						'Permissions are required to access the microphone, you can open the phone settings',
						[
							{
								text: 'OK',
							},
							{
								text: 'Settings',
							},
						],
					);
					setPermission(false);
				}
			}
		} catch (e) {
			Alert.alert('An error occurred', "We couldn't access your microphone");
			setPermission(false);
		}
	};

	const _onSpeechStart = useCallback(() => {
		console.log('onSpeechStart');
		setSearchText('');
	}, [setSearchText]);

	const _onSpeechEnd = (event) => {
		console.log('onSpeechEnd', event);
		setIsRecord(false);
	};

	const _onSpeechResults = useCallback(
		(event) => {
			console.log(event);
			console.log('onSpeechResults');
			setSearchText(event.value[0]);
		},
		[setSearchText],
	);

	const _onSpeechError = useCallback(({ error }) => {
		console.log('_onSpeechError');
		console.log(error);
	}, []);

	const _onSpeechPartialResults = useCallback(
		(event) => {
			console.log('_onSpeechPartialResults', event);
			setSearchText(event.value[0]);
		},
		[setSearchText],
	);

	const onRecordVoice = () => {
		if (isRecord) {
			Voice.stop();
		} else {
			Voice.start('en-US');
		}
		setIsRecord(!isRecord);
	};

	useEffect(() => {
		verifyPermissions();
	}, []);

	useEffect(() => {
		if (hasPermission) {
			Voice.onSpeechEnd = _onSpeechEnd;
			Voice.onSpeechError = _onSpeechError;
			Voice.onSpeechResults = _onSpeechResults;
			Voice.onSpeechStart = _onSpeechStart;
			Voice.onSpeechPartialResults = _onSpeechPartialResults;
		}

		return () => {
			Voice.destroy().then(Voice.removeAllListeners);
		};
	}, [
		_onSpeechPartialResults,
		_onSpeechResults,
		_onSpeechStart,
		hasPermission,
		_onSpeechError,
	]);

	return {
		onRecordVoice,
		setSearchText,
		isRecord,
		searchText,
		hasPermission,
	};
};

export default useSpeechSearch;
