import { StyleSheet } from 'react-native';
import Colors from './colors';
import spacing from './spacing';

const { m, xs, s } = spacing;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: m,
		backgroundColor: Colors.backgroundContainer,
	},
	center: {
		justifyContent: 'center',
	},
	button: {
		backgroundColor: 'green',
		padding: s,
		minWidth: 64,
		borderRadius: xs,
		marginHorizontal: s,
		marginBottom: m,
	},
	buttonText: {
		textAlign: 'center',
		color: 'white',
		textTransform: 'uppercase',
	},
	title: {
		fontSize: 19,
		fontWeight: '800',
		color: 'gray',
	},
	subtitle: {
		fontSize: 14,
		fontWeight: '500',
		color: 'black',
	},
	body: {
		fontSize: 12,
		color: 'black',
	},
	row: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
	},
	normalButton: {
		backgroundColor: 'cyan',
		borderRadius: m,
	},
});
export default styles;
