const spacing = {
	xl: 64,
	l: 32,
	m: 24,
	s: 16,
	xs: 8,
};
export default spacing;
