import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Icon from '@components/Icon';

export default function IconButton({
	style,
	disabled,
	onPress,
	title,
	...props
}) {
	return (
		<TouchableOpacity
			style={[styles.container, style, disabled ? styles.disabled : null]}
			disabled={disabled}
			onPress={onPress}
			accessibilityLabel={title}
			accessibilityRole={'button'}
			accessibilityState={{
				disabled: disabled,
			}}>
			<Icon size={24} {...props} />
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	container: {
		height: 44,
		width: 44,
		alignItems: 'center',
		justifyContent: 'center',
	},
	disabled: {
		opacity: 0.25,
	},
});
