import React from 'react';
import { Pressable, Text } from 'react-native';
import styles from '../../style';

const Button = ({ onPress, title, ...rest }) => {
	return (
		<Pressable style={styles.button} onPress={onPress} {...rest}>
			<Text style={styles.buttonText}>{title}</Text>
		</Pressable>
	);
};

export default Button;
