import React from 'react';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import EvilIconsIcon from 'react-native-vector-icons/EvilIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import MaterialIconsIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIconsIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import ZocialIcon from 'react-native-vector-icons/Zocial';
import SimpleLineIconsIcon from 'react-native-vector-icons/SimpleLineIcons';

export default function Icon({ set, ...props }) {
	switch (set) {
		case 'AntDesign':
			return <AntDesignIcon {...props} />;
		case 'Entypo':
			return <EntypoIcon {...props} />;
		case 'EvilIcons':
			return <EvilIconsIcon {...props} />;
		case 'Feather':
			return <FeatherIcon {...props} />;
		case 'FontAwesome':
			return <FontAwesomeIcon {...props} />;
		case 'Foundation':
			return <FoundationIcon {...props} />;
		case 'Ionicons':
			return <IoniconsIcon {...props} />;
		case 'MaterialIcons':
			return <MaterialIconsIcon {...props} />;
		case 'MaterialCommunityIcons':
			return <MaterialCommunityIconsIcon {...props} />;
		case 'Octicons':
			return <OcticonsIcon {...props} />;
		case 'Zocial':
			return <ZocialIcon {...props} />;
		case 'SimpleLineIcons':
			return <SimpleLineIconsIcon {...props} />;
		default:
			return <AntDesignIcon {...props} />;
	}
}
