import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import styles from '../../style';
const Card = ({ name, image, species, status }) => {
	return (
		<View style={customStyles.container}>
			<Image source={{ uri: image }} style={customStyles.image} />
			<Text style={styles.title}>{name}</Text>
			<Text style={styles.subtitle}>{species}</Text>
			<Text style={styles.body}>{status}</Text>
		</View>
	);
};
export default Card;

const customStyles = StyleSheet.create({
	image: {
		width: 80,
		height: 80,
	},
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 24,
	},
});
