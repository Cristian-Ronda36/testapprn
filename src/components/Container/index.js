import React from 'react';
import { View } from 'react-native';
import styles from '../../style';

const Container = ({ children, center, style }) => (
	<View style={[styles.container, center && styles.center, style]}>
		{children}
	</View>
);

export default Container;
