import React, { useRef } from 'react';
import { View, TextInput, StyleSheet, Animated } from 'react-native';
import IconButton from '../IconButton';

const VoiceInput = ({
	value,
	hintText,
	placeholder,
	isRecording,
	disabledVoiceSearch,
	onRecordVoice,
	...props
}) => {
	const opacity = useRef(new Animated.Value(0)).current;
	const translateY = useRef(new Animated.Value(-12)).current;
	const fadeIn = () => {
		Animated.parallel([
			Animated.timing(opacity, {
				toValue: 1,
				useNativeDriver: true,
				duration: 451,
			}),
			Animated.timing(translateY, {
				toValue: 0,
				useNativeDriver: true,
				duration: 451,
			}),
		]).start();
	};

	const fadeOut = () => {
		if (value.length !== 0) {
			return;
		}
		Animated.parallel([
			Animated.timing(opacity, {
				toValue: 0,
				useNativeDriver: true,
				duration: 451,
			}),
			Animated.timing(translateY, {
				toValue: -12,
				useNativeDriver: true,
				duration: 451,
			}),
		]).start();
	};
	const onFocus = () => {
		fadeIn();
	};

	const onBlur = () => {
		fadeOut();
	};

	return (
		<View style={styles.container}>
			<Animated.Text
				style={[
					styles.hintText,
					{ opacity: opacity },
					{ transform: [{ translateY: translateY }] },
				]}>
				{hintText}
			</Animated.Text>
			<TextInput
				value={value}
				style={styles.textInput}
				onFocus={onFocus}
				onBlur={onBlur}
				placeholder={placeholder}
				{...props}
			/>
			<IconButton
				onPress={onRecordVoice}
				disabled={disabledVoiceSearch}
				name="keyboard-voice"
				set="MaterialIcons"
				style={styles.icon}
				color={isRecording ? 'red' : 'black'}
			/>
		</View>
	);
};

export default VoiceInput;
const styles = StyleSheet.create({
	container: {
		position: 'relative',
		borderRadius: 12,
		flexDirection: 'row',
		alignItems: 'center',
		borderColor: 'black',
		borderWidth: StyleSheet.hairlineWidth,
		marginBottom: 8,
	},
	textInput: {
		flex: 1,
		paddingHorizontal: 22,
	},
	icon: { color: '#f0000022' },
	hintText: {
		position: 'absolute',
		top: -7,
		left: 16,
		zIndex: 2,
		paddingHorizontal: 10,
		backgroundColor: 'white',
		color: 'black',
	},
});
