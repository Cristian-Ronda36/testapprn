import React, { useState, forwardRef, useRef } from 'react';
import { TextInput, Animated, View, Text } from 'react-native';
import { MaskedTextInput } from 'react-native-mask-text';
import Colors from '../../style/colors';
import styles from './style';

const TextField = forwardRef((props, ref) => {
	const {
		autoFocus = false,
		keyboardType = 'default',
		maxLength,
		numberOfLines = 1,
		onSubmitEditing,
		returnKeyType,
		secureTextEntry = false,
		textContentType = 'none',
		icon,
		defaultValue,
		placeholder,
		focusCb = () => {},
		blurCb = () => {},
		onChangeText,
		value = '',
		style,
		placeholderTextColor = Colors.gray,
		isError = false,
		errorText,
		assistiveText,
		disabled = false,
		hintText,
		editable = true,
		countryCode,
		countryCallingCode,
		...additionalProps
	} = props;

	const [rowWrapperStyle, setRowWrapperStyle] = useState(styles.rowWrapper);
	const [focused, setFocused] = useState(false);
	const opacity = useRef(new Animated.Value(0)).current;
	const translateY = useRef(new Animated.Value(-12)).current;
	let bottomTextStyle = [styles.bottomText];

	const onFocus = () => {
		fadeIn();
		setRowWrapperStyle([styles.rowWrapper, { borderWidth: 2 }]);
		focusCb();
		setFocused(true);
	};

	const onBlur = () => {
		fadeOut();
		setRowWrapperStyle([styles.rowWrapper]);
		blurCb();
		setFocused(false);
	};

	if (isError) {
		bottomTextStyle = [...bottomTextStyle, styles.errorColor];
	}
	const fadeIn = () => {
		Animated.parallel([
			Animated.timing(opacity, {
				toValue: 1,
				useNativeDriver: true,
				duration: 451,
			}),
			Animated.timing(translateY, {
				toValue: 0,
				useNativeDriver: true,
				duration: 451,
			}),
		]).start();
	};

	const fadeOut = () => {
		if (value.length !== 0) {
			return;
		}
		Animated.parallel([
			Animated.timing(opacity, {
				toValue: 0,
				useNativeDriver: true,
				duration: 451,
			}),
			Animated.timing(translateY, {
				toValue: -12,
				useNativeDriver: true,
				duration: 451,
			}),
		]).start();
	};

	return (
		<View style={[styles.columnWrapper, style]}>
			<Animated.Text
				style={[
					styles.hintText,
					isError && styles.errorColor,
					disabled && { color: Colors.naturalGray },
					{ opacity: opacity },
					{ transform: [{ translateY: translateY }] },
				]}>
				{hintText || placeholder}
			</Animated.Text>
			<View
				style={[
					rowWrapperStyle,
					disabled && styles.disabled,
					isError && styles.error,
				]}>
				{textContentType !== 'telephoneNumber' ? (
					<View style={styles.iconContainer}>{icon}</View>
				) : (
					<View style={[styles.iconContainer, { flexDirection: 'row' }]}>
						<Text
							style={[
								{ color: Colors.gray },

								disabled && { color: Colors.naturalGray },
							]}>
							{`+${countryCallingCode}`}
						</Text>
					</View>
				)}
				<Input
					returnKeyType={returnKeyType}
					secureTextEntry={secureTextEntry}
					onChangeText={onChangeText}
					onSubmitEditing={onSubmitEditing}
					numberOfLines={numberOfLines}
					maxLength={maxLength}
					keyboardType={keyboardType}
					autoFocus={autoFocus}
					editable={!disabled && editable}
					textContentType={textContentType}
					defaultValue={defaultValue}
					style={[styles.textField, disabled && { color: Colors.naturalGray }]}
					onFocus={onFocus}
					onBlur={onBlur}
					placeholder={focused ? '' : placeholder}
					value={value}
					placeholderTextColor={
						disabled ? Colors.naturalGray : placeholderTextColor
					}
					accessibilityValue={{ text: value }}
					autoCorrect={false}
					autoCapitalize={textContentType !== 'emailAddress' ? null : 'none'}
					accessibilityLabel={placeholder}
					ref={ref}
					countryCode={countryCode}
					{...additionalProps}
				/>
			</View>
			<Text
				testID={
					isError ? `${props.testID}-error` : `${props.testID}-assistiveText`
				}
				style={[
					bottomTextStyle,
					isError && {
						marginBottom: 16,
					},
				]}>
				{isError ? errorText : assistiveText}
			</Text>
		</View>
	);
});

const Input = forwardRef((props, ref) => {
	if (props.textContentType === 'telephoneNumber') {
		return (
			<MaskedTextInput
				type="custom"
				mask={props.countryCode === 'US' ? '999-999-9999' : '9999999999'}
				keyboardType="numeric"
				{...props}
				ref={ref}
			/>
		);
	}
	if (props.textContentType === 'expirationDate') {
		return (
			<MaskedTextInput
				type={'custom'}
				options={{
					mask: '99/99',
				}}
				{...props}
				textContentType={'none'}
				ref={ref}
			/>
		);
	}
	return <TextInput {...props} ref={ref} />;
});
export default TextField;
