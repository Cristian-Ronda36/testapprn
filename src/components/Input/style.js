import { StyleSheet } from 'react-native';
import Colors from '../../style/colors';
const styles = StyleSheet.create({
	textField: {
		marginLeft: 12,
		flex: 1,
		color: Colors.gray,
		textAlignVertical: 'center',
	},
	columnWrapper: {
		flexDirection: 'column',
	},
	rowWrapper: {
		height: 54,
		borderWidth: 1,
		borderRadius: 4,
		flexDirection: 'row',
		alignItems: 'center',
		borderColor: Colors.navyBlue,
		backgroundColor: Colors.white,
		alignSelf: 'stretch',
	},
	iconContainer: {
		height: 54,
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 15,
	},
	hintText: {
		position: 'absolute',
		top: -7,
		left: 16,
		zIndex: 2,
		paddingHorizontal: 10,
		backgroundColor: Colors.backgroundContainer,
		color: Colors.navyBlue,
	},
	error: {
		borderColor: Colors.errorRed,
	},
	errorColor: {
		color: Colors.errorRed,
	},
	bottomText: {
		marginLeft: 16,
		marginTop: 6,
		color: Colors.gray,
	},
	disabled: {
		borderColor: Colors.naturalGray,
	},
	cardConnect: {
		flex: 1,
		alignSelf: 'stretch',
		marginLeft: 12,
	},
});

export default styles;
