# Config the project with Prettier and ESLint

1. Copy .eslintrc.js and .prettierrc.js
2. Install dependencies of Husky

```
yarn add -D husky
```

3. Set a assets in `react-native.config.js` in root folder

```js
module.exports = {
	project: {
		ios: {},
		android: {},
	},
	assets: ['./src/assets/fonts/'],
};
```

4. Add `env.js` in root folder
5. Add in `.gitignore`

```
### Config ###
env.js
versionInfo.js
```

## Test

For the autocomplete in testing use

```
yarn add -D @testing-library/jest-native @types/jest @testing-library/react-native
```
