# Libraries for start

This command installs [React Navigation and Stack](https://reactnavigation.org/docs/stack-navigator), [Masked Text](https://github.com/akinncar/react-native-mask-text#readme)

```
yarn add @react-navigation/native react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view @react-navigation/stack react-native-mask-text
```

## Libraries for Asyncstorage and Keychain

```
yarn @react-native-async-storage/async-storage react-native-keychain
```

**Don't forget after each install**

```
npx pod-install ios
```
