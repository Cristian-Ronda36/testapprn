# Example of package.json

```json
{
	"name": "name",
	"version": "X.Y.Z",
	"private": true,
	"scripts": {
		"android": "react-native run-android --variant=devDebug --appId com.company.nameapp",
		"ios": "react-native run-ios",
		"start": "react-native start",
		"test": "jest",
		"lint": "eslint ./src --max-warnings 0",
		"lint-fix": "eslint . --fix",
		"config-dev": "./scripts/configure.sh dev",
		"config-qa": "./scripts/configure.sh qa",
		"config-prod": "./scripts/configure.sh prod",
		"postinstall": "npx jetifier",
		"prettier": "prettier --write 'src/**/*.js' '__tests__/**/*.js'",
		"sync-i18n": "sync-i18n --files '**/i18n/*.json' --primary en --languages es --space 2"
	},
	"dependencies": {
		//   more dependencies
	},
	"devDependencies": {
		//   more devDependencies
		"@testing-library/jest-native": "^4.0.1",
		"@testing-library/react-native": "^7.2.0"
	},
	"jest": {
		"preset": "react-native"
	},
	"husky": {
		"hooks": {
			"pre-commit": "npm run lint"
		}
	}
}
```
