module.exports = {
	root: true,
	extends: '@react-native-community',
	rules: {
		'react-native/no-inline-styles': 0,
		'no-shadow': 0,
		'no-tabs': 0,
	},
	env: {
		jest: true,
	},
};
